CREATE DATABASE Tarea_SA;
use Tarea_SA;

CREATE TABLE cursos (
  codigo VARCHAR(20),
  nombre VARCHAR(10)
);

INSERT INTO cursos
  (codigo, nombre)
VALUES
  ('770', 'ipc1'),
  ('771', 'ipc2'),
  ('780', 'SA');
